// pages/phonepost/phonepost.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

    getInput:null

  },


  //获取手机号
  getInput: function (options){
    this.data.getInput = options.detail.value
  },

  //请求
  phoneNum: function (options){

    let phone = this.data.getInput

    wx.request({
      url: "https://www.ekaoyan365.com/api/user/checkExist",
      method: "POST",
      data: {
        phone: phone
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log(res.data);
     
      }
    })

  },




  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})