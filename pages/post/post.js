var postsDate = require('../../data/posts-data.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
  
        
  
  },


  /**
   * 生命周期函数--监听页面加载    on 开头的都为监听函数
   */
  onLoad: function (options) {
    // 页面初始化   options为页面跳转来的参数


    this.setData({
      // 因为要平铺传到 data中，所以要将 格式变为 key：键值 
      listArrLey: postsDate.postList,  
      banner: postsDate.banner
      })

  },

  onPostTap:function(event){

    console.log(event)
    
    var postId = event.currentTarget.dataset.postid;

    wx.navigateTo({
      url: 'post-detail/post-detail?id=' + postId
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 页面渲染完成
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //页面显示
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    //页面隐藏
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    //页面关闭
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
     
  }
})