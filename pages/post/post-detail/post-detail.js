var postsDate = require('../../../data/posts-data.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    var postId = options.id;

    this.data.currentPostId = postId;
    
    var postDate = postsDate.postList[postId]

    console.log(postDate)

    //this.data.postDate = postDate

      this.setData({
        postDate: postDate
      })
      
      var postsCollected = wx.getStorageSync('posts_ollected')
      if(postsCollected){
        var postCollected = postsCollected[postId]
        this.setData({
          collected:postCollected
        })
      }else{
        var postsCollected = {}
        postsCollected[postId]= false;
        wx.setStorageSync('posts_ollected', postsCollected)
      }

  },

  onCatchTapShou:function(event){
    var postsCollected = wx.getStorageSync('posts_ollected');
    var postCollected = postsCollected[this.data.currentPostId];
    // 收藏变成未收藏，为收藏变成收藏
    postCollected = !postCollected;
    postsCollected[this.data.currentPostId] = postCollected;
    //更新了文章是否收藏的缓存值
    

     this.showToastFun(postsCollected, postCollected);
    //this.showModalFun(postsCollected, postCollected);

  },

  onCatTapShare:function(){

    wx.showActionSheet({
      itemList: ['分享到QQ', '分享到微信', '分享到微博'],
      success(res) {
        console.log(res.tapIndex)
      },
      fail(res) {
        console.log(res.errMsg)
      }
    })

  },


  // showModalFun: function (postsCollected, postCollected){

  //   var that =  this; //  改变this指向
  //   wx.showModal({
  //     title: '收藏',
  //     content: postCollected?"收藏该文章":"取消收藏该文章",
  //     showCancel: true,
  //     cancelText: '取消',
  //     cancelColor: '#333',
  //     confirmText: '确认',
  //     confirmColor: '#405f80',
  //     success: function (res) {
  //         if(res.confirm){
  //             wx.setStorageSync('posts_ollected', postsCollected)
  //             // 更新数据绑定变量，从而实现切换样式
  //             that.setData({
  //               collected: postCollected
  //             })

  //         }
  //      }
  //   })
  // },

  showToastFun: function (postsCollected, postCollected) {

    wx.setStorageSync('posts_ollected', postsCollected)
    // 更新数据绑定变量，从而实现切换样式
    this.setData({
      collected: postCollected
    })

    wx.showToast({
      title: postCollected ? "收藏成功" : "取消成功",
      duration: 400,
      icon: 'success'  // 默认为：success  loading
    })

  },

  //  播放按钮
  onMusicTap:function(event){
    
    wx.playBackgroundAudio({
      dataUrl: '',
      title:'',
      coverImgUrl:''
    })
    
  },

 

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})